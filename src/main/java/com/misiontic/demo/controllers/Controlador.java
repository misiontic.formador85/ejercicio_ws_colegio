package com.misiontic.demo.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.demo.dao.CursoRepo;
import com.misiontic.demo.dao.EstudianteRepo;
import com.misiontic.demo.dto.Autenticacion;
import com.misiontic.demo.entities.Curso;
import com.misiontic.demo.entities.Estudiante;

@RestController
public class Controlador {

    @Autowired
    private CursoRepo cursoRepo;

    @Autowired
    private EstudianteRepo estudianteRepo;

    @PostMapping("/curso")
    public Curso crearOModificarCurso(@RequestBody Curso nuevoCurso){
        return cursoRepo.save(nuevoCurso);
    }

    @GetMapping("/cursos")
    public Iterable<Curso> getCursos(){
        return cursoRepo.findAll();
    }

    @GetMapping("/get-curso")
    public Curso getCursoById(@RequestParam int id){
        Optional<Curso> curso = cursoRepo.findById(id);
        return curso.get();
    }

    @GetMapping("/get-curso-by-nombre")
    public List<Curso> getCursoByNombre(@RequestParam String nombre){
        List<Curso> curso = cursoRepo.findByNombreCursoContaining(nombre);
        return curso;
    }

    @GetMapping("/borrar-curso")
    public void borrarCurso(@RequestParam int id){
        cursoRepo.deleteById(id);
    }

    @PostMapping("/auth")
    public void autenticacion(@RequestBody Autenticacion autenticacion ){
        System.out.println("CLAVE: " + autenticacion.getClave());
        System.out.println("USUARIO: " + autenticacion.getUsername());

        //Estudiante estudiante = estudianteRepo.findByUsuario(autenticacion.getUsername());

        //if 

    }

    
}
