package com.misiontic.demo.dto;

import lombok.Data;

@Data
public class Autenticacion {
    
    private String username;
    private String clave;
}
