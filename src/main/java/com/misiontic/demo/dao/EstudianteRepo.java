package com.misiontic.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.misiontic.demo.entities.Estudiante;

public interface EstudianteRepo extends CrudRepository<Estudiante,Integer> {
    
}
