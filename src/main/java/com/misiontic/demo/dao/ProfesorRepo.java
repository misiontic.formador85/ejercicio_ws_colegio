package com.misiontic.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.misiontic.demo.entities.Profesor;

public interface ProfesorRepo extends CrudRepository<Profesor,Integer> {
    
}
