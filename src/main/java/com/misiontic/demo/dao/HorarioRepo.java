package com.misiontic.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.misiontic.demo.entities.Horario;

public interface HorarioRepo extends CrudRepository<Horario,Integer> {
    
}
