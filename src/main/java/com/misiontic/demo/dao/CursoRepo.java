package com.misiontic.demo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.misiontic.demo.entities.Curso;

public interface CursoRepo extends CrudRepository<Curso,Integer> {

    List<Curso> findByNombreCursoContaining(String nombreCurso);

}
