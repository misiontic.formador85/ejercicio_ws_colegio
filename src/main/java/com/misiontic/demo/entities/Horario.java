package com.misiontic.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Horario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String horaInicio;
    @Column
    private String horaFin;
    @ManyToOne
    @JoinColumn
    private Curso curso;
    @ManyToOne
    @JoinColumn
    private Profesor profesor;

}
