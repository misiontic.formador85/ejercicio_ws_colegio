package com.misiontic.demo.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCurso;
    @Column
    private String nombreCurso;
    @Column
    private String codigoCurso;
    @Column
    private String intensidadHoraria;

    @OneToMany(mappedBy = "curso")
    private List<Estudiante> listaEstudiantes;
    
}
