package com.misiontic.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String usuario;
    @Column
    private String clave;
    @Column
    private String nombres;
    @Column
    private String apellidos;
    @Column
    private String fechaNacimiento;
    @ManyToOne
    @JoinColumn
    private Curso curso;

}
